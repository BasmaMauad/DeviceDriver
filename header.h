#ifndef HEADER_H_   /* Include guard */
#define HEADER_H_

///struct SystemThreadInformation
typedef struct _SYSTEM_THREADS
{
    LARGE_INTEGER           KernelTime;
    LARGE_INTEGER           UserTime;
    LARGE_INTEGER           CreateTime;
    ULONG                           WaitTime;
    PVOID                           StartAddress;
    CLIENT_ID                       ClientIs;
    KPRIORITY                       Priority;
    KPRIORITY                       BasePriority;
    ULONG                           ContextSwitchCount;
    ULONG                           ThreadState;
    KWAIT_REASON            WaitReason;
}SYSTEM_THREAD_INFORMATION, *PSYSTEM_THREAD_INFORMATION;

///struct SystemProcessInformation
typedef struct _SYSTEM_PROCESSES
{
    ULONG                           NextEntryDelta;
    ULONG                           ThreadCount;
    ULONG                           Reserved[6];
    LARGE_INTEGER           CreateTime;
    LARGE_INTEGER           UserTime;
    LARGE_INTEGER           KernelTime;
    UNICODE_STRING          ProcessName;
    KPRIORITY                       BasePriority;
    ULONG                           ProcessId;
    ULONG                           InheritedFromProcessId;
    ULONG                           HandleCount;
    ULONG                           Reserved2[2];
    VM_COUNTERS                     VmCounters;
    struct _SYSTEM_THREADS          Threads[1];
}SYSTEM_PROCESS_INFO, *PSYSTEM_PROCESS_INFO;

#endif

