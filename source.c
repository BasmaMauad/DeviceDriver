#include"ntddk.h"
#include "header.h"

// ZwQuerySystemInformation is routine that return a list of current processes
NTSTATUS ZwQuerySystemInformation(ULONG InfoClass, PVOID Buffer, ULONG Length, PULONG ReturnLength);

//////Driver//////
VOID unload(IN PDRIVER_OBJECT DriveObject)
{
	DbgPrint("BYE \n");

}

//main ,IN>>input argument (optional)
NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriveObject, IN UNICODE_STRING REGISTRYPATH)
{
	DriveObject->DriverUnload = unload;
	//DbgPrint("hello \n");  //printf
	PSYSTEM_PROCESS_INFO pSpi;
	PVOID buffer;
	buffer = ExAllocatePool(NonPagedPool, 1024 * 1024);  //malloc, allocate a block of memory and returns a pointer to the allocated block

														 //check if not reserved
	if (!buffer)
	{
		DbgPrint("ERROR1 \r\n");
	}

	/*NTSTATUS return type of routine and it's values are divided into four types:
	success values, informational values, warnings, and error values.*/

	//check the success value of ZwQuerySystemInformation routine.
	if (!NT_SUCCESS(ZwQuerySystemInformation(5, buffer, 1024 * 1024, NULL)))
	{
		ExFreePool(buffer); //free the allocated block
		DbgPrint("ERROR2 \r\n");
	}
	else
	{
		pSpi = (PSYSTEM_PROCESS_INFO)buffer;
		while (1)
		{
			DbgPrint("Current process is %wZ  %u \r\n", pSpi->ProcessName, pSpi->ProcessId); //display current processes, name and ID.
			if (pSpi->NextEntryDelta == 0)
			{
				break;
			}
			pSpi = (PSYSTEM_PROCESS_INFO)((unsigned char*)pSpi + pSpi->NextEntryDelta); //point to next place.
		}

	}

	ExFreePool(buffer); //free the allocated block
	return STATUS_SUCCESS;
}
